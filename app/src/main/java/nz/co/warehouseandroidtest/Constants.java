package nz.co.warehouseandroidtest;

public class Constants {
    public static final String HTTP_URL_ENDPOINT = "https://twg.azure-api.net/";
    public static final String PREF_USER_ID = "userId";
    public static final int BRANCH_ID = 208;
    public static final String SUBSCRIPTION_KEY = "";   // Please fill in the SUBSCRIPTION_KEY given to you here.
    public static final String MACHINE_ID = "1234567890";
}
